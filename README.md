# Magh

Ensemble logiciel et technique pour le spectacle *Magh.

## Checklist

- PC
- Leap-Motion
- Caches sacs et cache électronique
- Treuil
- Electronique treuil
- Alimentation treuil
- Câble 12V long doré treuil
- Bobine de câble
- Multiprise
- Routeur wifi
- Boite quincaillerie
  - tournevis cruciforme
  - ciseaux
  - pinces
  - serflex
- Kinect
- Adaptateur Kinect

## Logiciels à installer

- Python xy
- Node js (zeromq, express, socket.io)
- Kinect
- Leap-Motion
- librairie python zmq,keyboard,pykinect2
- chrome

## Installation sur scène

I) Repérage

1) Reperer deux rues libres sur le gril, parallèles direction jardin-cour
2) Placer le treuil au sol à Jardin, si possible au milieu des deux rues libres

II) Installation préalable

1) Installer le PC sur une table
2) Installer le routeur Wifi
3) Brancher l'electronique treuil au treuil
4) Brancher l'électronique treuil au câble doré
5) Brancher le câble doré à l'alimentation 12V
6) Brancher l'alimentation 12V au secteur
7) Lancer le logiciel de contrôle de la machine
8) Tester les rotations des 4 moteurs

III) Installation accastillage

1) Dérouler les tambours d'une longueur de la hauteur sous plafond pour liberer le peigne
2) Attacher le peigne à une barre en hauteur à la verticale du treuil pour former la colonne.
3) A partir du peigne, attacher chaque poulie des boucles de levage à chaque point d'attache du gril suivant le plan ci-dessous
4) ...
5) Attacher les contrepoids aux boucles de levage
6) Attachez chaque bout au tapis au centre

IV) Mesures et calibrations

1) à l'aide du télémètre laser, mesurer les dimensions du cube de travail
2) En déduire et marquer au sol le centre du cube
3) Les reporter dans le script MVCA.py
4) Positionner la Kinect sur son pied, brancher l'adaptateur
5) Mettre le son à fond du PC et lancer le logiciel de calibration, suivre les instructions

L'installation est donc prête.

## Mécanique

Tous les plans sont disponibles sur OnShape :
[Plans de la Machine Vivante version scène](https://cad.onshape.com/documents/7ca2a3b0865f47ca937a5730/w/463548946abbe7de7d45fb95/e/114a34648da549c01769bed3)

## programmation firmware
