socket = io.connect('127.0.0.1:80');

socket.on('disconnect', function () {
    socket.close();
});
var a;
var width = parseInt(document.getElementById("top").getAttributeNS(null,"width"));
var height = parseInt(document.getElementById("top").getAttributeNS(null,"height"));

function M2TopX(M,B,H){
    return -M[1]*0.9*width/B + width/2.;
}
function M2TopY(M,B,H){
    return M[0]*0.9*height/H + height/2.;
}
function M2FrontX(M,B,P){
    return -M[1]*0.9*width/B + width/2.;
}
function M2FrontY(M,B,P){
    return -M[2]*0.9*height/P + height;
}
socket.on('MVML', function (data) {
    document.getElementById("pos1").innerText = data.pos[0];
    document.getElementById("pos2").innerText = data.pos[1];
    document.getElementById("pos3").innerText = data.pos[2];
    document.getElementById("pos4").innerText = data.pos[3];
    var linked = data.winchLinked;
    if(linked){
        document.getElementById("connect").disabled = true;
        document.getElementById("disconnect").disabled = false;
    }else{
        document.getElementById("connect").disabled = false;
        document.getElementById("disconnect").disabled = true;
    }
    document.getElementById("tP1").setAttributeNS(null, 'cx',M2TopX(data.P1,data.B,data.H));
    document.getElementById("tP1").setAttributeNS(null, 'cy',M2TopY(data.P1,data.B,data.H));
    document.getElementById("tP2").setAttributeNS(null, 'cx',M2TopX(data.P2,data.B,data.H));
    document.getElementById("tP2").setAttributeNS(null, 'cy',M2TopY(data.P2,data.B,data.H));
    document.getElementById("tP3").setAttributeNS(null, 'cx',M2TopX(data.P3,data.B,data.H));
    document.getElementById("tP3").setAttributeNS(null, 'cy',M2TopY(data.P3,data.B,data.H));
    document.getElementById("tP4").setAttributeNS(null, 'cx',M2TopX(data.P4,data.B,data.H));
    document.getElementById("tP4").setAttributeNS(null, 'cy',M2TopY(data.P4,data.B,data.H));
    document.getElementById("tPleap").setAttributeNS(null, 'cx',M2TopX(data.leap,data.B,data.H));
    document.getElementById("tPleap").setAttributeNS(null, 'cy',M2TopY(data.leap,data.B,data.H));
    document.getElementById("tM").setAttributeNS(null, 'cx',M2TopX(data.M,data.B,data.H));
    document.getElementById("tM").setAttributeNS(null, 'cy',M2TopY(data.M,data.B,data.H));
    document.getElementById("tOBJ").setAttributeNS(null, 'cx',M2TopX(data.OBJ,data.B,data.H));
    document.getElementById("tOBJ").setAttributeNS(null, 'cy',M2TopY(data.OBJ,data.B,data.H));
    document.getElementById("thead").setAttributeNS(null, 'cx',M2TopX(data.head,data.B,data.H));
    document.getElementById("thead").setAttributeNS(null, 'cy',M2TopY(data.head,data.B,data.H));
    document.getElementById("tleftHand").setAttributeNS(null, 'cx',M2TopX(data.leftHand,data.B,data.H));
    document.getElementById("tleftHand").setAttributeNS(null, 'cy',M2TopY(data.leftHand,data.B,data.H));
    document.getElementById("trightHand").setAttributeNS(null, 'cx',M2TopX(data.rightHand,data.B,data.H));
    document.getElementById("trightHand").setAttributeNS(null, 'cy',M2TopY(data.rightHand,data.B,data.H));
    
    document.getElementById("fP1").setAttributeNS(null, 'cx',M2FrontX(data.P1,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fP1").setAttributeNS(null, 'cy',M2FrontY(data.P1,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fP2").setAttributeNS(null, 'cx',M2FrontX(data.P2,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fP2").setAttributeNS(null, 'cy',M2FrontY(data.P2,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fP3").setAttributeNS(null, 'cx',M2FrontX(data.P3,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fP3").setAttributeNS(null, 'cy',M2FrontY(data.P3,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fP4").setAttributeNS(null, 'cx',M2FrontX(data.P4,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fP4").setAttributeNS(null, 'cy',M2FrontY(data.P4,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fPleap").setAttributeNS(null, 'cx',M2FrontX(data.leap,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fPleap").setAttributeNS(null, 'cy',M2FrontY(data.leap,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fM").setAttributeNS(null, 'cx',M2FrontX(data.M,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fM").setAttributeNS(null, 'cy',M2FrontY(data.M,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fOBJ").setAttributeNS(null, 'cx',M2FrontX(data.OBJ,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fOBJ").setAttributeNS(null, 'cy',M2FrontY(data.OBJ,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fhead").setAttributeNS(null, 'cx',M2FrontX(data.head,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fhead").setAttributeNS(null, 'cy',M2FrontY(data.head,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fleftHand").setAttributeNS(null, 'cx',M2FrontX(data.leftHand,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("fleftHand").setAttributeNS(null, 'cy',M2FrontY(data.leftHand,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("frightHand").setAttributeNS(null, 'cx',M2FrontX(data.rightHand,data.B,Math.max(data.PA,data.PF)));
    document.getElementById("frightHand").setAttributeNS(null, 'cy',M2FrontY(data.rightHand,data.B,Math.max(data.PA,data.PF)));
    
    if(document.getElementById("Chore").length==0 || data.choreChanged){
        document.getElementById("Chore").innerHTML = ""
        for(var ch in data.chores){
            var opt = document.createElement("option")
            opt.value = ch
            opt.innerHTML = data.chores[ch]
            opt.addEventListener("click", function (input) {
                socket.emit('cmd', 'setchore|'+input.target.textContent);
                document.getElementById('choreTitle').value = input.target.textContent
            });
            document.getElementById("Chore").appendChild(opt)
        }
    }
    // build table
    if(data.poseChanged == true){
        var tab = document.getElementById("poseTable")
        tab.innerHTML = ""
        var header = tab.createTHead()
        var h=header.insertRow(0)
        var hname = h.insertCell(0)
        var hduration = h.insertCell(1)
        var hobjectif = h.insertCell(2)
        hname.innerHTML = "Name"
        hduration.innerHTML = "Duration"
        hobjectif.innerHTML = "Objective"
        for(ip in data.seq){
            var r = tab.insertRow()
            r.contentEditable = true
            r.setAttribute("onclick","socket.emit('cmd', 'setpose|'+this.rowIndex.toString()+'|'+this.cells[0].innerHTML+'|'+this.cells[1].innerHTML+'|'+this.cells[2].innerHTML);")
            var name = r.insertCell(0)
            name.innerHTML = data.seq[ip].Name
            var duration = r.insertCell(1)
            duration.innerHTML = data.seq[ip].Duration
            var objectif = r.insertCell(2)
            objectif.innerHTML = data.seq[ip].position
            
        }
        posesChanged = false;
    }
    //highlight current pose
    var tab = document.getElementById("poseTable")
    for(var ir = 0;ir < tab.rows.length;ir++){
        if(ir == data.nseq+1){
            tab.rows[ir].style.backgroundColor='red'
        }else{
            tab.rows[ir].style.backgroundColor='black'
        }
    }
    var wBar = document.getElementById("wBar")
    wBar.value = 100.0*data.W
    
    //console.log(data);
});