import zmq, time, json,os,csv
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from Kinetic import extractPoints
import Leap
import random
from numpy import *
import glob

def norm(A,B):
    return ((A[0]-B[0])**2+(A[1]-B[1])**2+(A[2]-B[2])**2)**0.5
# planar distance between two points
def normXY(A,B):
    return ((A[0]-B[0])**2+(A[1]-B[1])**2)**0.5
    
def bary(A,B,w):
    v = 1.-w
    return [A[0]*v+B[0]*w,A[1]*v+B[1]*w,A[2]*v+B[2]*w]

def isfloat(a):
    try:
        float(a)
        return True
    except:
        return False
    
def loadChore(fileName):
    a = []
    if not os.path.exists(str(fileName)):
        fileName = 'csv\_stop.csv'
    r = csv.DictReader(open(str(fileName),'r'),delimiter = ';')
    for row in r:
        a.append(row)
    return a
 
def saveChore(listpose,fileName):
        f = open(fileName,'w')
        head = ["Name","Duration","position"]
        r = csv.DictWriter(f,delimiter = ';',lineterminator = '\n',fieldnames=head)
        r.writeheader()
        for row in listpose:
            r.writerow(row)
        f.close()


os.system('START node app.js')
IPwinch = "10.0.0.114"

os.system('START chrome localhost')



ctx = zmq.Context()
web = ctx.socket(zmq.SUB)
web.bind('tcp://*:2000')
web.setsockopt(zmq.SUBSCRIBE,'')
pol = zmq.Poller()
pol.register(web,zmq.POLLIN)

mvmlpub = ctx.socket(zmq.PUB)
mvmlpub.bind('tcp://*:3000')
mvml = {}

winchpub = ctx.socket(zmq.PUB)
winchpub.connect('tcp://'+IPwinch+':8080')


# Parametres geometriques
# largeur de scene (m)
mvml["B"] = 6.0
# profondeur de scene (m)
mvml["H"] = 6.0
# hauteur sous perche en arriere scene (m)
mvml["PF"] = 5.9
# hauteur sous perche en avant scene (m)
mvml["PA"] = 5.9
# zero de la Leap Motion (plus c'est grand, plus le zero est haut)
zeroLeap = 3.0

# gains des treuils
kdist = -1.0
pasParMetre1 = 217.0*kdist
pasParMetre2 = 217.0*kdist
pasParMetre3 = 218.0*kdist
pasParMetre4 = 211.0*kdist
id1 = 0
id2 = 1
id3 = 2
id4 = 3
speed_treuil = 100
dt = 0.04
margin = 0.0

leapController = Leap.Controller()

k = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Body | PyKinectV2.FrameSourceTypes_Depth)
width = k.depth_frame_desc.Width
height = k.depth_frame_desc.Height
#inclinaison de la kinect (tilt)
tilt=-1.3*pi/180.0
#hauteur de la kinect (hkinect ou mesurer si 0)
w=0.83
#orientation de la kinect (pan attention negatif)
pan=-86.51*pi/180.0
#distance de la kinect par rapport au centre (dkinect)
d=3.6
print "Kinect lance"

# calcul des points particuliers
mvml["P1"] = [-mvml["H"]/2.   ,-mvml["B"]/2.,mvml["PF"]]
mvml["P2"] = [-mvml["H"]/2.   , mvml["B"]/2.,mvml["PF"]]
mvml["P3"] = [ mvml["H"]/2.   ,-mvml["B"]/2.,mvml["PA"]]
mvml["P4"] = [ mvml["H"]/2.   , mvml["B"]/2.,mvml["PA"]]
L10 = norm([0.,0.,0.],mvml["P1"])
L20 = norm([0.,0.,0.],mvml["P2"])
L30 = norm([0.,0.,0.],mvml["P3"])
L40 = norm([0.,0.,0.],mvml["P4"])
mvml["APEX"] = [0.,0.,(mvml["PA"]+mvml["PF"])/4]
mvml["M"] = mvml["APEX"]
mvml["OBJ"] = mvml["APEX"]
mvml["INI"] = mvml["APEX"]
mvml["W"] = 1.
mvml["leap"] = [0.,0.,0.]
mvml["head"] = mvml["APEX"]
mvml["leftHand"] = mvml["APEX"]
mvml["rightHand"] = mvml["APEX"]
mvml["pos"] = [0,0,0,0]
mvml["delta"] = [0,0,0,0]
mvml["chores"] = glob.glob("csv/*.csv")
mvml["chore"] = "csv/_stop.csv"
mvml["seq"] = loadChore(mvml["chore"])
mvml["nseq"] = 0
mvml["continous"] = False
mvml["winchLinked"] = False
mvml["timeratio"] = 1.
finger_t = [0.,0.,0.]


while True:
    # capteurs
    t = time.clock()
    
    # leap
    frame = leapController.frame()
    #pinch = 0.0
    thumb_ext = False
    index_ext = False
    middle_ext = False
    ring_ext = False
    pinky_ext = False
    for fin in frame.fingers:
        if fin.hand.is_left and fin.type == fin.TYPE_THUMB:
            thumb_ext = fin.is_extended
        if fin.hand.is_left and fin.type == fin.TYPE_INDEX:
            # leap en position ds le public
            #finger_t[0] = fin.tip_position.z/50.0
            #finger_t[1] = fin.tip_position.x/50.0
            #finger_t[2] = -2.0+fin.tip_position.y/50.0
            # leap a jardin
            finger_t[0] = fin.tip_position.x/50.0
            finger_t[1] = fin.tip_position.z/50.0
            finger_t[2] = -zeroLeap+fin.tip_position.y/50.0
            #pinch = fin.hand.pinch_strength
            index_ext = fin.is_extended
        if fin.hand.is_left and fin.type == fin.TYPE_MIDDLE:
            middle_ext = fin.is_extended
        if fin.hand.is_left and fin.type == fin.TYPE_RING:
            ring_ext = fin.is_extended
        if fin.hand.is_left and fin.type == fin.TYPE_PINKY:
            pinky_ext = fin.is_extended
    mvml["leap"][0] = 0.9*mvml["leap"][0]+0.1*finger_t[0]
    mvml["leap"][1] = 0.9*mvml["leap"][1]+0.1*finger_t[1]
    mvml["leap"][2] = 0.9*mvml["leap"][2]+0.1*finger_t[2]
    mvml["delta"] = [0,0,0,0]
    
    # kinect
    if k.has_new_body_frame():
        bs = k.get_last_body_frame()
        if bs is not None:
            for b in bs.bodies:
                if b.is_tracked:
                    js = b.joints
                    kpos = extractPoints(js,tilt,w,pan,d)
                    if (0.5*kpos["r_toe"][2]+0.5*kpos["l_toe"][2])<0.5:
                        a=kpos["head"].tolist()
                        mvml["head"][0] = a[0]
                        mvml["head"][1] = -a[1]
                        mvml["head"][2] = a[2]
                        a=kpos["l_tip"].tolist()
                        mvml["leftHand"][0] = a[0]
                        mvml["leftHand"][1] = -a[1]
                        mvml["leftHand"][2] = a[2]
                        a=kpos["r_tip"].tolist()
                        mvml["rightHand"][0] = a[0]
                        mvml["rightHand"][1] = -a[1]
                        mvml["rightHand"][2] = a[2]
    # interface
    mvml["poseChanged"] = False
    mvml["choreChanged"] = False
    socks = dict(pol.poll(0))
    if socks.get(web) == zmq.POLLIN:
        t = web.recv()
        mvml["order"] = t.split('|')
        print mvml["order"]
        if mvml["order"][0] == 'setchore':
            mvml["chore"] = mvml["order"][1]
            mvml["seq"] = loadChore(mvml["chore"])
            mvml["nseq"] = 0
            mvml["INI"] = mvml["M"]
            mvml["W"] = 0.
            mvml["chores"] = glob.glob("csv/*.csv")
            mvml["poseChanged"] = True
            mvml["choreChanged"] = True
        elif mvml["order"][0] == 'save':
            saveChore(mvml["seq"],mvml["order"][1])
            mvml["chores"] = glob.glob("csv/*.csv")
            mvml["choreChanged"] = True
        elif mvml["order"][0] == 'setpose':
            mvml["nseq"] = int(mvml["order"][1])-1
            if len(mvml["order"][2])>0:
                mvml["seq"][mvml["nseq"]]["Name"] = mvml["order"][2]
            if isfloat(mvml["order"][3]):
                mvml["seq"][mvml["nseq"]]["Duration"] = mvml["order"][3]
            if len(mvml["order"][4])>0:
                mvml["seq"][mvml["nseq"]]["position"] = mvml["order"][4]
            mvml["INI"] = mvml["M"]
            mvml["W"] = 0.
            mvml["poseChanged"] = True
        elif mvml["order"][0] == 'copy below':
            mvml["seq"].insert(mvml["nseq"]+1,mvml["seq"][mvml["nseq"]].copy())
            mvml["nseq"] +=1
            mvml["INI"] = mvml["M"]
            mvml["W"] = 0.
            mvml["poseChanged"] = True
        elif mvml["order"][0] == 'delete':
            mvml["seq"].pop(mvml["nseq"])
            print mvml["nseq"],len(mvml["seq"])
            if mvml["nseq"] >= len(mvml["seq"]):
                mvml["nseq"] = len(mvml["seq"])-1
            mvml["INI"] = mvml["M"]
            mvml["W"] = 0.
            mvml["poseChanged"] = True
        elif mvml["order"][0] == 'put leap below':
            mvml["seq"].insert(mvml["nseq"]+1,{"Name":"leap_acq","Duration":"1","position":"X,"+"{0:.2f},".format(mvml["leap"][0])+"{0:.2f},".format(mvml["leap"][1])+"{0:.2f}".format(mvml["leap"][2])})
            mvml["nseq"] +=1
            mvml["INI"] = mvml["M"]
            mvml["W"] = 0.
            mvml["poseChanged"] = True
        elif mvml["order"][0] == 'acq leap below':
            mvml["seq"][mvml["nseq"]]["position"]="X,"+"{0:.2f},".format(mvml["leap"][0])+"{0:.2f},".format(mvml["leap"][1])+"{0:.2f}".format(mvml["leap"][2])
            mvml["INI"] = mvml["M"]
            mvml["W"] = 0.
            mvml["poseChanged"] = True    
        elif mvml["order"][0] == 'play':
            mvml["continous"] = True
        elif mvml["order"][0] == 'timeRatio':
            mvml["timeratio"] = float(mvml["order"][1])/100.
        elif mvml["order"][0] == 'speedMax':
            speed_treuil = int(int(mvml["order"][1])*255/100)    
        elif mvml["order"][0] == 'pause':
            mvml["continous"] = False
        elif mvml["order"][0] == 'stop':
            req = [{"id":id1,"ordre":"vit","param":0.0},{"id":id2,"ordre":"vit","param":0.0},{"id":id3,"ordre":"vit","param":0.0},{"id":id4,"ordre":"vit","param":0.0},{"id":4,"Vmax":speed_treuil,"K":4,"Ki":0.0}]
            winchpub.send(json.dumps(req))
        elif mvml["order"][0] == 'up1':
            req = [{"id":id1,"ordre":"vit","param":speed_treuil}]
            winchpub.send(json.dumps(req))
        elif mvml["order"][0] == 'dn1':
            req = [{"id":id1,"ordre":"vit","param":-speed_treuil}]
            winchpub.send(json.dumps(req))
        elif mvml["order"][0] == 'up2':
            req = [{"id":id2,"ordre":"vit","param":speed_treuil}]
            winchpub.send(json.dumps(req))
        elif mvml["order"][0] == 'dn2':
            req = [{"id":id2,"ordre":"vit","param":-speed_treuil}]
            winchpub.send(json.dumps(req))
        elif mvml["order"][0] == 'up3':
            req = [{"id":id3,"ordre":"vit","param":speed_treuil}]
            winchpub.send(json.dumps(req))
        elif mvml["order"][0] == 'dn3':
            req = [{"id":id3,"ordre":"vit","param":-speed_treuil}]
            winchpub.send(json.dumps(req))
        elif mvml["order"][0] == 'up4':
            req = [{"id":id4,"ordre":"vit","param":speed_treuil}]
            winchpub.send(json.dumps(req))
        elif mvml["order"][0] == 'dn4':
            req = [{"id":id4,"ordre":"vit","param":-speed_treuil}]
            winchpub.send(json.dumps(req))
        elif mvml["order"][0] == 'connect':
            mvml["winchLinked"] = True
        elif mvml["order"][0] == 'disconnect':
            mvml["winchLinked"] = False
            req = [{"id":id1,"ordre":"vit","param":0.0},{"id":id2,"ordre":"vit","param":0.0},{"id":id3,"ordre":"vit","param":0.0},{"id":id4,"ordre":"vit","param":0.0},{"id":4,"Vmax":speed_treuil,"K":4,"Ki":0.0}]
            winchpub.send(json.dumps(req))
        elif mvml["order"][0] == 'raz':
            req = [{"id":id1,"ordre":"raz","param":0},{"id":id2,"ordre":"raz","param":0},{"id":id3,"ordre":"raz","param":0},{"id":id4,"ordre":"raz","param":0}]
            winchpub.send(json.dumps(req))
    
    # controle
    pose = mvml["seq"][mvml["nseq"]]
    if pose["position"][0] == 'M':
        mvml["OBJ"] = mvml["M"]
    elif pose["position"][0] == 'X':
        coord = pose["position"].split(',')
        if isfloat(coord[1]) and isfloat(coord[2]) and isfloat(coord[3]):
            mvml["OBJ"] = [float(coord[1]),float(coord[2]),float(coord[3])]
        else:
            mvml["OBJ"] = mvml["M"]
    elif pose["position"][0] == 'D':
        coord = pose["position"].split(',')
        if len(coord)==5 and isfloat(coord[1]) and isfloat(coord[2]) and isfloat(coord[3]) and isfloat(coord[4]):
            mvml["delta"] = [float(coord[1]),float(coord[2]),float(coord[3]),float(coord[4])]
    elif pose["position"][0] == 'L':
        mvml["OBJ"] = mvml["leap"]
        # figures des mains
        if thumb_ext and not index_ext and not middle_ext and not ring_ext and not pinky_ext:
            mvml["delta"] = [0,200,0,200]
        if thumb_ext and index_ext and middle_ext and ring_ext and pinky_ext:
            mvml["delta"] = [0,0,0,0]
        if thumb_ext and index_ext and not middle_ext and not ring_ext and not pinky_ext:
            mvml["delta"] = [200,200,50,0]
        if thumb_ext and index_ext and middle_ext and not ring_ext and not pinky_ext:
            mvml["delta"] = [0,0,300,300]    
    elif pose["position"][0] == 'H':
        mvml["OBJ"] = mvml["head"]
        mvml["OBJ"][2] = 2.0
    elif pose["position"][0] == 'S':
        mvml["OBJ"] = mvml["head"]
        mvml["OBJ"][0] = -mvml["head"][0]
        mvml["OBJ"][1] = mvml["head"][1]
        mvml["OBJ"][2] = mvml["head"][2]
    elif pose["position"][0] == 'T':
        mvml["OBJ"] = mvml["rightHand"]
        mvml["OBJ"][2] = 2.0
    elif pose["position"][0] == 'R':
        mvml["OBJ"] = mvml["leftHand"]
        mvml["OBJ"][2] = 2.0
    elif pose["position"][0] == 'F':
        if normXY(mvml["M"],mvml["head"])<1.:
            mvml["OBJ"] = [(random.random()-0.5)*6.,(random.random()-0.5)*6.,2.0]
    
    mvml["W"] = min(1., mvml["W"]+dt/(float(pose["Duration"])/mvml["timeratio"]))
    mvml["M"] = bary(mvml["INI"],mvml["OBJ"],mvml["W"])
    if mvml["continous"] == True:
        if mvml["W"] == 1. and mvml["nseq"]<len(mvml["seq"])-1:
            mvml["nseq"] +=1
            if mvml["seq"][mvml["nseq"]]["Name"] == "restart":
                mvml["nseq"] = 0
            mvml["INI"] = mvml["M"]
            mvml["W"] = 0.
            
    # commande
    # limitation de M
    if mvml["M"][2]<=0.:
        mvml["M"][2] = 0.
    if mvml["M"][2]>=(mvml["PA"]+mvml["PF"])/2-margin:
        mvml["M"][2] = (mvml["PA"]+mvml["PF"])/2-margin
        
    if mvml["M"][0]<=(-mvml["H"]/2.)+margin:
        mvml["M"][0] = (-mvml["H"]/2.)+margin
    if mvml["M"][0]>=(mvml["H"]/2.)-margin:
        mvml["M"][0] = (mvml["H"]/2.)-margin
        
    if mvml["M"][1]<=(-mvml["B"]/2.)+margin:
        mvml["M"][1] = (-mvml["B"]/2.)+margin
    if mvml["M"][1]>=(mvml["B"]/2.)-margin:
        mvml["M"][1] = (mvml["B"]/2.)-margin

    mvml["pos"][0] = int(pasParMetre1*(norm(mvml["M"],mvml["P1"])-L10))+mvml["delta"][0]
    mvml["pos"][1] = int(pasParMetre2*(norm(mvml["M"],mvml["P2"])-L20))+mvml["delta"][1]
    mvml["pos"][2] = int(pasParMetre3*(norm(mvml["M"],mvml["P3"])-L30))+mvml["delta"][2]
    mvml["pos"][3] = int(pasParMetre3*(norm(mvml["M"],mvml["P4"])-L40))+mvml["delta"][3]

    req = [ {"id":id1,"ordre":"pos","param":mvml["pos"][0]},
            {"id":id2,"ordre":"pos","param":mvml["pos"][1]},
            {"id":id3,"ordre":"pos","param":mvml["pos"][2]},
            {"id":id4,"ordre":"pos","param":mvml["pos"][3]},
            {"id":4,"Vmax":speed_treuil,"K":4.0,"Ki":0.0}]
    if mvml["winchLinked"] and len(req)>0:
        winchpub.send(json.dumps(req))
    mvmlpub.send(json.dumps(mvml))
    time.sleep(dt)